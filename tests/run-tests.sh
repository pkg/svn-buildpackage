#!/bin/sh

set -e
set -x

TESTREPO=$1
SVNWORKDIR=$2
if [ -z "${TESTREPO}" ]; then
	echo "Specify the local / private test root URL."
	exit 800
fi
SANITY=`echo ${TESTREPO} | grep svn.debian || true`
if [ -n "$SANITY" ]; then
	echo "Please use a local repo, not a Debian one!"
	exit 900
fi
SANITY=`echo ${TESTREPO} | grep svn.alioth || true`
if [ -n "$SANITY" ]; then
	echo "Please use a local repo, not an Alioth one!"
	exit 1000
fi
# make sure we've got a test package here
parsechangelog >/dev/null
DIR=`dirname $0`
LOGFILE=`tempfile`
# each test can fail but we still go on to the next one.
set +e
${DIR}/tags.sh ${TESTREPO} ${SVNWORKDIR} | tee ${LOGFILE}
${DIR}/tag-only.sh ${TESTREPO} ${SVNWORKDIR} | tee -a ${LOGFILE}
echo "Logs are in ${LOGFILE}"
FAIL=`grep "Fail for svn-buildpackage" ${LOGFILE} || true`
COUNT=`grep -c "Fail for svn-buildpackage" ${LOGFILE} || true`
if [ -n "$FAIL" ]; then
	echo $FAIL
	exit $COUNT
fi
