			SVN-BUILDPACKAGE TEST SUITE README
			==================================

Initial tests focus on tags (which themselves are just branches, but that's
a different problem).

I'm imposing a requirement on all future uploads of svn-buildpackage
whilst I'm maintainer - a local test suite:

0. Everyone needs to have svn commit access to a dummy repository which
should not be public. I use one on my own server because that makes
sure I can test svn+ssh:// connections, others can use file://
connections. The repository does not need to be retained, indeed it
should be cleaned out completely from time to time, starting again
from revision 0.

1. The test scripts themselves will live in svn-buildpackage SVN but
won't be part of the binary package, just the source.

2. The tests will involve a test package, only ever committed to your
local test repo and which can contain whatever you like. A native
package containing empty shell script(s) is going to be quick to build
and quick to test, so that's one good plan.

3. Changes to the test package will serve as excuses to bump the
changelog to test svn-bp tagging. To make that easier, the test package
can be deliberately lintian dirty and incomplete. If you would normally
make 20 changes to a bare directory to make a suitable package, you
have 20 revisions to build and tag.

4. The test suite itself will simply be a wrapper around
svn-buildpackage --svn-tag and --svn-retag --svn-tag-only with and
without the SVNBPPERLLIB variable set and the test script will
deliberately call dch -i and debcommit to force new versions of the
package. The test script will take it's current working directory
location to set SVNBPPERLLIB because you'll be calling it from the test
package build directory (which could well be in /tmp somewhere - mine
will).

5. Future issues like this will need new tests devised.

6. I will ensure that all my uploads pass these tests and I henceforth
expect that other uploaders will respect this test suite by ensuring
that their uploads pass the tests as well. We must have no more
ping-pong bugs. I will *not* be happy with anyone who uploads a version
that fails the test suite - I might even consider reverting the changes
in the uploaded version and uploading a replacement version, including
reinstating the "closed" bugs. Versions that fail the test suite will
not get into testing.

7. Tests performed by the test suite need to declare which bugs in the
BTS are being tested, to identify regressions. For bugs to become test
cases, the bug report needs to have a clear, reproducible, way to test
the issue.

The test suite should be simple shell, using set -x, so that logs can
be created that show exactly what is being tested.
