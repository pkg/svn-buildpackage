#!/bin/sh
#
# svn-do - run a command within an exported tree
# Copyright (C) 2006-2007 Lo�c Minier <lool@dooz.org>
# Copyright 2010 Neil Williams <codehelp@debian.org>
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
# SOFTWARE IN THE PUBLIC INTEREST, INC. BE LIABLE FOR ANY CLAIM, DAMAGES OR
# OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
# ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
# DEALINGS IN THE SOFTWARE.
#
# Except as contained in this notice, the name of the author shall not be used
# in advertising or otherwise to promote the sale, use or other dealings in
# this Software without prior written authorization from the author.
#
# depends: svn-buildpackage

set -e

OLD_WD=$(pwd)

while [ -n "$1" ]; do
case "$1" in
	--)
	while [ -n "$1" ]
	do
		shift
		OUR_OPTS="${OUR_OPTS} $1 "
	done
	continue
	;;
	*)
	CMD="$CMD $1 "
	shift
	continue
	;;
esac
done

if ! svn info >/dev/null 2>&1 ; then
    echo "E: Not in a SVN checkout" >&2
    exit 1
fi

if ! dh_testdir; then
    echo "E: Not in a Debian source tree" >&2
    exit 1
fi

echo "I: Exporting source tree via svn-buildpackage..." >&2
echo "I: svn-buildpackage --svn-dont-clean --svn-export --svn-no-links --svn-ignore ${OUR_OPTS}"
log=$(mktemp)
set +e
LC_ALL=C svn-buildpackage --svn-dont-clean --svn-export --svn-no-links --svn-ignore ${OUR_OPTS} >"$log" 2>&1
set -e
tree=$(LC_ALL=C cat "$log" | sed -n 's/.*exported to \(.*\)/\1/p')

if [ -z "$tree" ]; then
       cat "$log"
       rm "$log"
    echo "E: Export failed, check your svn-buildpackage configuration"
    exit 1
fi
rm "$log"

# uncomment this if you want to purge the exported tree on completion
# (successful or not)
#cleanup() {
#    echo "I: Cleaning up..." >&2
#    rm -rf "$*"
#}
#trap "cleanup $tree" 0 1 2 3 9 11 13 15

cd "$tree"
if [ -z "$CMD" ]; then
	[ "$SHELL" ] || SHELL=/bin/sh
	echo "I: No command given, running default command: $SHELL"
	echo "I: type exit or use Control-D to exit this sub-shell."
	(eval "$SHELL")
else
	echo "I: Running command: ${CMD}"
	(eval "${CMD}")
fi

err=$?

if [ 0 != $err ]; then
    echo "E: command exited with $err; not copying back the debian/ tree." >&2
    exit 1
fi

echo "I: Copying back the debian/ tree..." >&2
cp -vuapf debian/* "$OLD_WD/debian"
